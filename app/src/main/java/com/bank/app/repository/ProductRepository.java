package com.bank.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bank.app.domain.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {
	
	Product findById(int id);
	
	List<Product> findAll();
	
	
	@Query("SELECT p FROM Product p WHERE p.name LIKE %?1%")
	List<Product> findAllLikeName(String name);
}
