package com.bank.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bank.app.domain.SerialNumber;

public interface SerialNumberRepository extends JpaRepository<SerialNumber, Integer>{

	SerialNumber findBySerialnumber(String serialNumber);

}
