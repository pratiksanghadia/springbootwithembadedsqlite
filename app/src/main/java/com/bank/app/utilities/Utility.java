package com.bank.app.utilities;

import java.util.Scanner;

import org.springframework.stereotype.Component;
@Component
public class Utility {
	
	public String getSerialNumber() throws Throwable {
        Process process = Runtime.getRuntime().exec(new String[] { "wmic", "bios", "get", "serialnumber" });
        process.getOutputStream().close();
        Scanner sc = new Scanner(process.getInputStream());
        String property = sc.next();
        String serial = sc.next();
        System.out.println(property + ": " + serial);
        return serial;
    }
}
