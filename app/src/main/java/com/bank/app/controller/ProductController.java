package com.bank.app.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bank.app.domain.Product;
import com.bank.app.model.Search;
import com.bank.app.service.ProductService;

@Controller
public class ProductController {
	
	private final Logger logger = LoggerFactory.getLogger(ProductController.class);
	
	@Autowired
	ProductService productService; 
	
	@RequestMapping(value = {"/","/products"}, method = RequestMethod.GET)
    public ModelAndView getAllProducts() {
        ModelAndView model = new ModelAndView("main");
        List<Product> productList = productService.findAll();
        
        model.addObject("productList", productList);
        model.addObject("includePage", "/WEB-INF/jsp/products.jsp");

        return model;
    }
	
	@RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
    public ModelAndView getProductByID(@PathVariable("id") int id) {
		
        ModelAndView model = new ModelAndView("main");
        
        Product product = productService.findById(id);
        
        model.addObject("product", product);
        model.addObject("includePage", "/WEB-INF/jsp/product.jsp");

        return model;
    }
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)
    public ModelAndView search(@Valid @ModelAttribute("search") Search search, BindingResult result) {
        ModelAndView model = new ModelAndView("main");
        
        if(result.hasErrors()){
            logger.error("Error occurs while searching: {}", result.getAllErrors());
            model.addObject("includePage", "/WEB-INF/jsp/search.jsp");
            return model;
        }

        List<Product> searchResult = productService.caseSensitiveSearchResult(search.getSearchText().trim());
        System.out.println("data --- "+searchResult.size());
        model.addObject("searchResult", searchResult);
        model.addObject("searchText", search.getSearchText());
        model.addObject("includePage", "/WEB-INF/jsp/search.jsp");
        
        return model;
    }
	
	@ModelAttribute
    public void addingCommonObjects(Model model) {
//        model.addAttribute("user", new User());      //It needs for springForm:form tag
        model.addAttribute("search", new Search());  //It needs for springForm:form tag
    }
	
	@PostMapping("/addProduct")
	@ResponseBody
	public Product addProduct(@RequestBody Product product){
		return productService.addProduct(product);
	}
	
	@GetMapping("getAllProduct")
	@ResponseBody
	public List<Product> getAllProduct(){
		return productService.getAllProduct();
	}
}
