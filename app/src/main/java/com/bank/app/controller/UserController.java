package com.bank.app.controller;

import java.security.Principal;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bank.app.domain.Product;
import com.bank.app.domain.User;
import com.bank.app.model.Search;
import com.bank.app.service.UserService;

@Controller
public class UserController {
	
	@Autowired
	private UserService userService; 
	
	@RequestMapping("/")
	public ModelAndView helloWorld() {
		ModelAndView model = new ModelAndView("main");
		return model;
	}
	 
	@PostMapping("/addUser")
	@ResponseBody
	public User addUser(@RequestBody User user){
		return userService.addUser(user);
	}
	
	@RequestMapping(value = "/changePassword", method = RequestMethod.GET)
    public ModelAndView changePassword() {
        ModelAndView model = new ModelAndView("main");
        model.addObject("includePage", "/WEB-INF/jsp/changePassword.jsp");
        return model;
    }
	
	@PostMapping("/changePasswordPost")
	public String changePassword(@RequestParam("oldPassword") String oldPassword, @RequestParam("newPassword") String newPassword,
			Principal principal,RedirectAttributes redirectAttributes) {
		User user = userService.findUsername(principal.getName());
		if(Objects.nonNull(user) && Objects.nonNull(oldPassword) && Objects.nonNull(newPassword) && user.getPassword().equals(oldPassword)){
			user.setPassword(newPassword);
			userService.addUser(user);
			return "redirect:/products";
		}
		redirectAttributes.addAttribute("changePasswordError", "Invalid password");
		return "redirect:/changePassword";
	}

	@ModelAttribute
    public void addingCommonObjects(Model model) {
//        model.addAttribute("user", new User());      //It needs for springForm:form tag
        model.addAttribute("search", new Search());  //It needs for springForm:form tag
    }
}
