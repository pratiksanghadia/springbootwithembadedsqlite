package com.bank.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.bank.app.service.SerialNumberService;
import com.bank.app.utilities.Utility;

@SpringBootApplication
public class AppApplication extends SpringBootServletInitializer implements CommandLineRunner {

	@Autowired
	Utility utility;
	
	@Autowired
	SerialNumberService serialNumberServicec;
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(AppApplication.class);
    }

	public static void main(String[] args) {
		SpringApplication.run(AppApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		try {
			System.out.println("::::::::::::::;;;"+utility.getSerialNumber());
			Boolean isSerialNumberExist = serialNumberServicec.isSerialNumberAvailable(utility.getSerialNumber());
			if(!isSerialNumberExist){
				//System.exit(1);
			}
		} catch (Throwable e) {
			e.printStackTrace();
			System.exit(1);
		}
		
	}
	
}
