package com.bank.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.bank.app.domain.User;
import com.bank.app.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	public User findUsername(String name) {
		return userRepository.findByUsername(name);
	}

	public User addUser(User user) {
		user.setPassword( bCryptPasswordEncoder.encode(user.getPassword()));
		return userRepository.save(user);
	}

}
