package com.bank.app.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.app.domain.SerialNumber;
import com.bank.app.repository.SerialNumberRepository;

@Service
public class SerialNumberService {

	@Autowired
	SerialNumberRepository serialNumberRepository;
	
	public Boolean isSerialNumberAvailable(String serialNumber){
		SerialNumber serialNumberObj = serialNumberRepository.findBySerialnumber(serialNumber);
		return Objects.nonNull(serialNumberObj);
	}
}
