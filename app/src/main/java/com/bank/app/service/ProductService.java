package com.bank.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.bank.app.domain.Product;
import com.bank.app.repository.ProductRepository;

@Service
public class ProductService {
	
	private final Logger logger = LoggerFactory.getLogger(ProductService.class);
	
	 @Autowired
	 private ProductRepository productRepository;
	 
	 public List<Product> findAll(){
		 
		 return productRepository.findAll();
	 }
	 
	 public Product findById(int id){
		 
		 return productRepository.findById(id);
	 }

	public List<Product> caseSensitiveSearchResult(String searchText) {
		if(StringUtils.isEmpty(searchText)){
			return productRepository.findAll();
		}
		return productRepository.findAllLikeName(searchText);
    }

	public Product addProduct(Product product) {
		return productRepository.save(product);
	}

	public List<Product> getAllProduct() {
		return productRepository.findAll();
	}

}
