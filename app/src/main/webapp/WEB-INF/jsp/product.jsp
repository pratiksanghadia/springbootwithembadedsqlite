<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>

<div class="panel panel-primary">
    <div class="media-body" style="padding-left: 10px;">
    	<label>Name: </label>
    	<h5 class="panel-title" style="right: 10px; font-size: 14px;">
    		${product.name}
    	</h5>
    	<br>
    	<label>Catogery: </label>
    	<h5 class="panel-title" style="right: 10px; font-size: 14px;">
    		${product.catogery}
    	</h5>
    	<br>
    	<label>Description: </label>
    	<h5 class="panel-title" style="right: 10px; font-size: 14px;">
    		${product.description}
    	</h5>
    </div>
</div>
