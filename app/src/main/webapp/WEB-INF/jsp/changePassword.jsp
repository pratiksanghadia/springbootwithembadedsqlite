<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script> 
          
    // Function to check Whether both passwords 
    // is same or not. 
    function checkPassword(form) { 
        password1 = form.newPassword.value; 
        password2 = form.confirmPassword.value; 
        document.getElementById("newPassword").style.display = "none";
        document.getElementById("confirmNewPassword").style.display = "none";
        // If password not entered 
        if (password1 == '') {
        	document.getElementById("newPassword").style.display = "block";
        	document.getElementById("newPassword").innerHTML = "Please enter Password";
        }
        // If confirm password not entered 
        else if (password2 == '') { 
        	document.getElementById("confirmNewPassword").style.display = "block";
        	document.getElementById("confirmNewPassword").innerHTML = "Please enter confirm password";
        }    
        // If Not same return False.     
        else if (password1 != password2) { 
            document.getElementById("confirmPassword").focus();
            document.getElementById("confirmNewPassword").style.display = "block";
            document.getElementById("confirmNewPassword").innerHTML = "Password did not match: Please try again";
        } 

        // If same return True. 
        else {
        	document.getElementById("confirmNewPassword").style.display = "none";
        	document.getElementById("errorDiv").style.display = "none";
        	$('#changePassword').removeAttr("disabled");
            return true; 
        } 
    } 
</script>
<form action="changePasswordPost" method="post" id="passwordChange" style="padding: 1%;">
	
	<table style="max-width: max-content;">
		<tr>
			<td style="padding: 1%;">Old Password: </td>
			<td style="padding: 1%;"><input type="password" name="password" /></td>
		</tr>
		<tr>
			<td style="padding: 1%;">New Password: </td>
			<td style="padding: 1%;"><input type="password" name="newPassword" id="newPassword"/></td>
			<p style="font-size: 20; color: #FF1C19;" id="newPassword" style="display:none" align="center"></p>
		</tr>
		<tr>
			<td style="padding: 1%;">Confirm New Password: </td>
			<td style="padding: 1%;"><input type="password" name="confirmPassword" id="confirmPassword" onfocusout="checkPassword(passwordChange)"/></td>
			<p style="font-size: 20; color: #FF1C19;" id="confirmNewPassword" style="display:none" align="center"></p>
		</tr>
	</table>
	<div>
		<button type="submit" class="btn btn-primary" value="Change Password"  id="changePassword" disabled="disabled" style="margin-left: 38%;">
			Change Password
		</button>
		<!-- <button type="submit" class="btn btn-primary" value="Cancel">Cancel</button> -->
	</div>
	
</form>