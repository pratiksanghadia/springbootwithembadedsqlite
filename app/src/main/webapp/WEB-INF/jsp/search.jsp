<%-- 
    Document   : search
    Created on : Mar 21, 2017, 2:03:34 PM
    Author     : USER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<style>
    .search-content {
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;

        line-height: 16px;     /* fallback */
        max-height: 32px;      /* fallback */
        -webkit-line-clamp: 2; /* number of lines to show */

        /*        
        line-height: 18px;      
        max-height: 54px;       
        -webkit-line-clamp: 3;   
        */

        -webkit-box-orient: vertical;
    }
    .panel {
        margin-bottom: 5px !important; 
    }
    .list-group-item{
        background-image: url(<c:url value="/img/background.png" />) !important;
    }
</style>


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Found ${searchResult.size()} result for keyword <span class="label label-default" style="font-size: 90%;"><c:out value="${searchText}" escapeXml="true"/></span></h3>
    </div>
</div>

<c:if test="${not empty searchResult}">
	<ul id="search-list">
	    <c:forEach var="product" items="${searchResult}">
	    	<div class="panel panel-primary">
	            <div class="media-body" style="padding-left: 10px;">
	                <c:if test="${not empty product.name}">
	                	<%-- <h4 class="panel-title" style="right: 10px; font-size: 14px;" ONCLICK="GetProductData(${product.id})">
	                		${product.name}
	                	</h4> --%>
	                	<li>
	                		<a type="label" href="/product/${product.id}">${product.name}</a>
	                	</li>
					<%--<div style="bottom: 0px; margin-bottom: 0px;">
	                    	<a type="label" href="/product/${product.id}">${product.name}</a>
	                    </div> --%>
	                </c:if>
	            </div>
	        </div>
	    </c:forEach>
    </ul>
</c:if>


<%-- <script src="<c:url value="/js/paginathing.js" />"></script> --%>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('.list-group').paginathing({
            perPage: 10,
//            limitPagination: false,
            limitPagination: 10,
            prevNext: true,
            firstLast: true,
            prevText: '&laquo;',
            nextText: '&raquo;',
            firstText: 'First',
            lastText: 'Last',
            containerClass: 'panel-footer',
            ulClass: 'pagination',
            liClass: 'page',
            activeClass: 'active',
            disabledClass: 'disabled'
        });

    });
</script>







