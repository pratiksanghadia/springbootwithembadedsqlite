<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>HDFC ERGO</title>
    <script src="<c:url value="/js/jquery.min.js" />"></script>
    <script src="<c:url value="/js/bootstrap.min.js" />"></script>
    <link  href="/css/bootstrap.min.css" rel="stylesheet" />
    <link  href="/css/bootstrap-theme.min.css" rel="stylesheet" />
    <link  href="/css/login.css" rel="stylesheet" />
</head>
<body style="overflow-y: scroll; margin-bottom: 35px; background-image: url(<c:url value="/img/background.png" />);">
	<div class="container" style="max-width: 960px;">
		<div class="container-fluid" style="padding:20%">
			<form action="login" method="post" class="login-form text-center">
				<c:if test="${param.error}">
		            <div align="center">
		                <p style="font-size: 20; color: #FF1C19;">Username or Password invalid, please verify</p>
		            </div>
		        </c:if>

				<table>
					<tbody>
						<tr>
							<td style="padding: 5%; width: 5%;">UserName: </td>
							<td style="padding: 5%; width: 5%;"><input type="text" name="username" /></td>
						</tr>
							<tr>
							<td style="padding: 5%; width: 5%;">Password: </td>
							<td style="padding: 5%; width: 5%;"><input type="password" name="password" /></td>
						</tr>
					</tbody>
				</table>
				<div>
					<button type="submit" class="btn btn-primary" value="Login" >Login</button>
				</div>
			</form>
		</div>
	</div>
</body>
</html>