<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<c:set var="contextpath" value="${pageContext.request.contextPath}"></c:set>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        
        <c:url var="home" value="/" scope="request" />

        <title>Products</title>
        
        <script src="<c:url value="/js/jquery3.min.js" />"></script>
        <script src="<c:url value="/js/bootstrap3.min.js" />"></script>
        <link  href="/css/bootstrap3.min.css" rel="stylesheet" />
        <link  href="/css/bootstrap3-theme.min.css" rel="stylesheet" />
        <link  href="/css/login.css" rel="stylesheet" />
        <link  href="/css/simplePagination.css" rel="stylesheet" />
        
    </head>
    <body style="overflow-y: scroll; margin-bottom: 35px; background-image: url(<c:url value="/img/background.png" />);">
        <div class="container" style="max-width: 960px;">
            <!-- Header -->
            <nav class="navbar navbar-default">
	            <div class="container-fluid">
				   <div style="margin-left: 67%;">
				    	<springForm:form method="POST" modelAttribute="search" action="${home}search">
	                        <div class="input-group">
	                            <springForm:input path="searchText" cssClass="form-control" placeholder="Search for..." />
	                            <span class="input-group-btn">
	                                <input id="searchBtn" type="submit" class="btn btn-primary" value="Search"  />
	                            </span>
	                        </div>
	                        <springForm:errors path="searchText" cssClass="error" cssStyle="color: #ff0000; font-style: italic;" />
                    	</springForm:form>
				    </div>
				</div>
            </nav>
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
				      <a class="navbar-brand" href="${contextpath}/products">Policy List</a>
				    </div>
				    <div class="pull-right">
						<div class="dropdown" style="padding-top: 21%;">
						  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
						  <span class="caret"></span></button>
						  <ul class="dropdown-menu">
						    <li><a href="${home}">Policy List</a></li>
						    <li><a href="${home}changePassword">Change Password</a></li>
						  </ul>
						</div>
					</div>
				</div>
			</nav>
            <div class="row" style="height: 550px;">
                <!-- left area -->
                <div id="main-content" class="col-lg-12">
                    <jsp:include page="${includePage}" />
                </div>
            </div>

            <footer style='background: rgba(0,0,0,.6); height: 30px; bottom: 0; padding: 5px; '>
                <div class="container" style="max-width: 930px; color: rgba(249,249,249,0.7);">
                    <spring:message code="main.footer"/>
                </div>
            </footer>
        </div>
    </body>
</html>