<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="contextpath" value="${pageContext.request.contextPath}"></c:set>
<!DOCTYPE html>
<script src="${contextpath}/js/jquery.simplePagination.js"></script>
<div class="col-xs-12">
	<table class="table">
	<thead>
		<tr>
			<th>Name</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<c:if test="${not empty productList}">
			<c:forEach var="product" items="${productList}">
			<tr>
				<td>${product.name}</td>
				<td>
					<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal" onclick="openPolicyDetail()">Open</button>
					<button type="button" class="btn btn-seccess btn-xs">Approve</button> 
				</td>
			</tr>
			</c:forEach>
		</c:if>
	</tbody>
	</table>
	<div id="pager" class="pull-right">
	     <ul id="pagination" class="pagination-sm"></ul>
	</div>
</div>

<jsp:include page="policyDetails.jsp"></jsp:include>
<%-- <c:if test="${not empty productList}">
	<ul style="height: 500px;">
	    <c:forEach var="product" items="${productList}">
	    	<div class="panel panel-primary">
	            <div class="media-body" style="padding-left: 10px;">
	                <c:if test="${not empty product.name}">
	                	<h4 class="panel-title" style="right: 10px; font-size: 14px;" ONCLICK="GetProductData(${product.id})">
	                		${product.name}
	                	</h4>
	                	<li>
	                		<a type="label" href="/product/${product.id}">${product.name}</a>
	                	</li>
					<div style="bottom: 0px; margin-bottom: 0px;">
	                    	<a type="label" href="/product/${product.id}">${product.name}</a>
	                    </div>
	                </c:if>
	            </div>
	        </div>
	    </c:forEach>
    </ul>
</c:if> --%>

<script>
var pagecontext = "${contextpath}";
$("#pagination").pagination({
    items: 1000,
    itemsOnPage: 10,
    currentPage:5,
    cssStyle: 'light-theme',
    onPageClick: function loadData(pageNumber,event){
    	console.log(event);
    	var offset = Math.max(pageNumber - 1, 0) * 10;
    	console.log("limit:"+10+" offset:"+offset+" page:"+parseInt(event.target.hash.split("-")[1]));
    }
});

function openPolicyDetail(){
	
}
</script>